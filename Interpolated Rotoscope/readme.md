# Interpolated Rotoscope
The film was shot digitally and then animated using interpolated rotoscope, an animation technique in which animators trace over the original footage frame by frame, for use in live-action and animated films, giving the finished result a distinctive animated look.
- https://en.wikipedia.org/wiki/A_Scanner_Darkly_(film)